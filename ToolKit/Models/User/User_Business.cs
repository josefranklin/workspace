﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ToolKit.Models.User
{
    public class User_Business
    {
        public int BusinessId { get; set; }
        public int UserId { get; set; }
    }
}