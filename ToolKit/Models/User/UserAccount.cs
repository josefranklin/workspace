﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ToolKit.Models.User
{
    public class LoginObj
    {
        public int Id { get; set; }
        public int WorkId { get; set; }
        public string Password { get; set; }
    }
}