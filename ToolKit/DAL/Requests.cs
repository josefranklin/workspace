﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ToolKit.Models.User;

namespace ToolKit.Connections
{
    public class Requests
    {
            public string connectionString = "Data Source=(localdb)\\ProjectsV13;Initial Catalog=toolkit;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            public LoginObj LoginUser(int workId, string password)
            {
                var modelLogin = new LoginObj();
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = @"SELECT * FROM User_Login WHERE work_id= @workId and password= @password";
                    command.Parameters.AddWithValue("@workId", workId);
                    command.Parameters.AddWithValue("@password", password);
                    SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        modelLogin.WorkId = workId;
                        modelLogin.Password = password.ToString();
                        modelLogin.Id = Convert.ToInt32(reader["id_user"]);

                    }
                }
                else
                {
                }

            }
                return modelLogin;
                
            }

            public UserInfo InfoUser(int Id)
            {
                var modelUser = new UserInfo();
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = @"SELECT User_Login.id_user, User_Login.work_id, User_Login.[password], [User].[name] as NomeUsuario,[User].[email] as EmailUsuario, [User].[Phone] as FoneUsuario, User_Business.id_business as IdBusiness, Business.[description] as DescBusiness " +
                            "FROM (((User_Login " +
                            "INNER JOIN [User] ON User_Login.id_user = [User].id_user) " +
                            "INNER JOIN User_Business ON User_Business.id_user = [User].id_user) " +
                            "INNER JOIN Business ON User_Business.id_business = Business.id_business) " +
                            "WHERE User_Login.id_user= @Id";

                    command.Parameters.AddWithValue("@Id", Id);
                    SqlDataReader reader = command.ExecuteReader();

                var ListaBusiness = new List<Business>();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            modelUser.Name = Convert.ToString(reader["NomeUsuario"]);
                            modelUser.Email = Convert.ToString(reader["EmailUsuario"]);
                            modelUser.Phone = Convert.ToString(reader["FoneUsuario"]);

                            var Dropdow = new Business();
                            Dropdow.Id = Convert.ToInt32(reader["IdBusiness"]);
                            Dropdow.Description = Convert.ToString(reader["DescBusiness"]);
                            ListaBusiness.Add(Dropdow);
                        
                            modelUser.Id = Id;
                        }

                        modelUser.Busines = ListaBusiness;


                }






                }
                return modelUser;
            }

        /*public void CreateSomething(ViewModel model)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand("", connection))
            {
                command.CommandText = "insert into Names values(@Name)";
                command.Parameters.AddWithValue("@Name", model.Name);
                command.ExecuteNonQuery();
            }
        }
        public void DeleteSomething(ViewModel model)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand("", connection))
            {
                command.CommandText = "delete from Names where Id=@Id";
                command.Parameters.AddWithValue("@Id", model.Id);
                command.ExecuteNonQuery();
            }
        }

        public void EditSomething(ViewModel model)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand("", connection))
            {
                command.CommandText = "Update Names set Name=@Name where Id=@Id";
                command.Parameters.AddWithValue("@Name", model.Name);
                command.Parameters.AddWithValue("@Id", model.Id);
                command.ExecuteNonQuery();
            }
    }*/
    }
}