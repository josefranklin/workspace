﻿using System;
using System.Data.SqlClient;
using System.Web.Mvc;
using ToolKit.Models.User;
using ToolKit.Connections;

namespace ToolKit.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View(new LoginObj());
        }

        [HttpPost]
        public ActionResult Verify(LoginObj login)
        {
            try
            {
                var request = new Requests();
                var logado = request.LoginUser(login.WorkId, login.Password);
                WebSession.UsuarioLogado = logado;
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View("Error");
            }
        }


    }
}