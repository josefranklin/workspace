﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToolKit.Models.User;
using ToolKit.Connections;

namespace ToolKit.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                int id = WebSession.UsuarioLogado.Id;
                var request = new Requests();
                var saida = request.InfoUser(id);
                return View(saida);
            }
            catch(Exception ex)
            {
                return View("Error");
            }
            
        }
       
    }
}