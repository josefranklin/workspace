﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToolKit.Models.User;

namespace ToolKit
{
    public class WebSession
    {
        public static LoginObj UsuarioLogado
        {
            get { return (LoginObj)HttpContext.Current.Session["SessionUsuarioLogado"]; }
            set { HttpContext.Current.Session["SessionUsuarioLogado"] = value; }
        }

        public static UserInfo UserInfo
        {
            get { return (UserInfo)HttpContext.Current.Session["SessionUserInfo"]; }
            set { HttpContext.Current.Session["SessionUserInfo"] = value; }
        }

        public static Business Business
        {
            get { return (Business)HttpContext.Current.Session["SessionBusiness"]; }
            set { HttpContext.Current.Session["SessionBusiness"] = value; }
        }
    }
}