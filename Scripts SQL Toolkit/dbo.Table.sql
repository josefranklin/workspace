﻿CREATE TABLE [dbo].[Table] (
    [id_user_role] INT IDENTITY (1, 1) NOT NULL,
    [id_user]      INT NOT NULL,
    [id_role]      INT NOT NULL,
    PRIMARY KEY CLUSTERED ([id_user_role] ASC),
    FOREIGN KEY ([id_user]) REFERENCES [dbo].[User] ([id_user])
);

