﻿CREATE TABLE [dbo].[Workplace] (
    [id_workplace] INT           NOT NULL,
    [description]  VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_workplace] ASC)
);

