﻿CREATE TABLE [dbo].[Squad] (
    [id_squad]   INT NOT NULL,
    [id_release] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([id_squad] ASC),
    FOREIGN KEY ([id_release]) REFERENCES [dbo].[Release_Train] ([id_release])
);

