﻿CREATE TABLE [dbo].[User_Business] (
    [id_user_business] INT IDENTITY (1, 1) NOT NULL,
    [id_user]          INT NOT NULL,
    [id_business]      INT NOT NULL,
    PRIMARY KEY CLUSTERED ([id_user_business] ASC),
    FOREIGN KEY ([id_user]) REFERENCES [dbo].[User] ([id_user]),
    FOREIGN KEY ([id_business]) REFERENCES [dbo].[Business] ([id_business])
);

