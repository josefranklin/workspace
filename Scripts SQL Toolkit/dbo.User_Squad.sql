﻿CREATE TABLE [dbo].[User_Squad] (
    [id_user_squad] INT IDENTITY (1, 1) NOT NULL,
    [id_user]       INT NOT NULL,
    [id_squad]      INT NOT NULL,
    PRIMARY KEY CLUSTERED ([id_user_squad] ASC),
    FOREIGN KEY ([id_user]) REFERENCES [dbo].[User] ([id_user]),
    FOREIGN KEY ([id_squad]) REFERENCES [dbo].[Squad] ([id_squad])
);

