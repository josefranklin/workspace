﻿CREATE TABLE [dbo].[User_Login] (
    [id_user_login] INT          IDENTITY (1, 1) NOT NULL,
    [id_user]       INT          NOT NULL,
    [work_id]       INT          NOT NULL,
    [password]      VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_user_login] ASC),
    FOREIGN KEY ([id_user]) REFERENCES [dbo].[User] ([id_user])
);

