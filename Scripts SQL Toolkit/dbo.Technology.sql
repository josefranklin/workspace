﻿CREATE TABLE [dbo].[Technology] (
    [id_technology] INT           NOT NULL,
    [description]   VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_technology] ASC)
);

