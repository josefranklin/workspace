﻿CREATE TABLE [dbo].[User] (
    [id_user] INT           IDENTITY (1, 1) NOT NULL,
    [name]    VARCHAR (50)  NOT NULL,
    [email]   VARCHAR (100) NOT NULL,
    [phone]   VARCHAR (12)  NOT NULL,
    PRIMARY KEY CLUSTERED ([id_user] ASC)
);

