﻿CREATE TABLE [dbo].[Business] (
    [id_business] INT          NOT NULL,
    [description] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_business] ASC)
);

