﻿CREATE TABLE [dbo].[Role] (
    [id_role]     INT           NOT NULL,
    [description] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_role] ASC)
);

