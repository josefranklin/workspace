﻿CREATE TABLE [dbo].[Release_Train] (
    [id_release]  INT           NOT NULL,
    [description] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_release] ASC)
);

