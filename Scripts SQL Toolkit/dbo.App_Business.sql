﻿CREATE TABLE [dbo].[App_Business] (
    [id_app_business] INT          NOT NULL,
    [id_business]     INT          NOT NULL,
    [description]     VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_app_business] ASC),
    FOREIGN KEY ([id_business]) REFERENCES [dbo].[Business] ([id_business])
);

