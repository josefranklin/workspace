﻿CREATE TABLE [dbo].[Technology_Type] (
    [id_technology_type] INT           IDENTITY (1, 1) NOT NULL,
    [id_technology]      INT           NOT NULL,
    [description]        VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_technology_type] ASC),
    FOREIGN KEY ([id_technology]) REFERENCES [dbo].[Technology] ([id_technology])
);

